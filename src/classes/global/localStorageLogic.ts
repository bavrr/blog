import { ReactionsLocalStorage } from "@/types/reaction/reactionInLocalStorage";
import { Blog } from "../blog/blog";
import { Reaction } from "../reaction/reaction";

class LocalStorageLogic {
  getBlogs(): Blog[] {
    const blogs = localStorage.getItem("blogList");

    if (!blogs) {
      return [];
    }

    return JSON.parse(blogs);
  }

  getBlog(id: string) {
    const blogs = this.getBlogs();
    return blogs.find((blog) => blog.id === id);
  }

  initBlogs(blogs: Blog[]) {
    localStorage.setItem("blogList", JSON.stringify(blogs));
  }

  initBlogsReactions() {
    const blogs = this.getBlogs();
    const blogsReactions: ReactionsLocalStorage = {};

    blogs.forEach((blog) => {
      blogsReactions[blog.id] = new Reaction().reaction;
    });

    localStorage.setItem("blogsReactions", JSON.stringify(blogsReactions));
  }

  getBlogReactions(blogID: string) {
    const blogsReactions = localStorage.getItem("blogsReactions");

    if (!blogsReactions) {
      return new Reaction();
    }

    const parsedBlogsReactions: ReactionsLocalStorage =
      JSON.parse(blogsReactions);

    return new Reaction(parsedBlogsReactions[blogID]);
  }

  setBlogReactions(blogID: string, blogReactions: Reaction) {
    const blogsReactions = localStorage.getItem("blogsReactions");
    let parsedBlogsReactions: ReactionsLocalStorage = {};

    if (blogsReactions) {
      parsedBlogsReactions = JSON.parse(blogsReactions);
    }

    parsedBlogsReactions[blogID] = blogReactions.reaction;

    localStorage.setItem(
      "blogsReactions",
      JSON.stringify(parsedBlogsReactions)
    );
  }
}

export const localStorageLogic = new LocalStorageLogic();
