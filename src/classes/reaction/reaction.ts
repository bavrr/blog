import { REACTION_LIST } from "@/helpers/const/reactionList";
import { ReactionIDs } from "@/types/reaction/reactionIds";

export class Reaction {
  reaction: ReturnType<typeof generateReaction>;

  constructor(reaction = generateReaction()) {
    this.reaction = reaction;
  }

  updateReaction(reactionID: ReactionIDs) {
    const reaction = this.reaction.find(
      (reaction) => reaction.id === reactionID
    );

    if (!reaction) {
      return;
    }

    reaction.count++;
  }
}

const generateReaction = () => {
  return REACTION_LIST.map((reaction) => {
    return {
      count: 0,
      ...reaction,
    };
  });
};
