export class Blog {
  id: string;
  title: string;
  publicationDate: string;
  img: string;
  announce: string;
  description: string;

  constructor(
    { id, title, publicationDate, img, announce, description }: Blog = {
      id: String(
        Math.floor(Date.now() / Math.floor(Math.random() * 100) + 150)
      ),
      title: `Заголовок ${Math.floor(Math.random() * 100)}`,
      publicationDate: new Date(
        Date.now() - Math.random() * 600000 * Math.random() * 345000
      ).toLocaleString("ru-RU"),
      img: `https://via.placeholder.com/150/000000/FFFFFF/?text=${Math.floor(
        Math.random() * 100
      )}`,
      announce:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    }
  ) {
    this.id = id;
    this.title = title;
    this.publicationDate = publicationDate;
    this.img = img;
    this.announce = announce;
    this.description = description;
  }
}
