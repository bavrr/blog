export const REACTION_LIST = [
  {
    id: "fire",
    value: "🔥",
  },
  {
    id: "heart",
    value: "❤️",
  },
  {
    id: "popper",
    value: "🎉",
  },
  {
    id: "laught",
    value: "😁",
  },
  {
    id: "scream",
    value: "😱",
  },
  {
    id: "star",
    value: "🤩",
  },
  {
    id: "sad",
    value: "😥",
  },
  {
    id: "dislike",
    value: "👎",
  },
  {
    id: "poo",
    value: "💩",
  },
  {
    id: "vomiting",
    value: "🤮",
  },
] as const;
