import { createRouter, createWebHistory } from "vue-router";
import BlogList from "@/pages/blogs/blogList/blogList.vue";
import BlogInformation from "@/pages/blogs/blogInformation/blogInformation.vue";

export const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "BlogList",
      component: BlogList,
    },
    {
      path: "/blog/:id",
      name: "BlogInformation",
      component: BlogInformation,
    },
  ],
});
