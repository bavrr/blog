import { REACTION_LIST } from "@/helpers/const/reactionList";

export type ReactionIDs = typeof REACTION_LIST[number]["id"];
