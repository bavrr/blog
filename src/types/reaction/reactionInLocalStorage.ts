import { Reaction } from "@/classes/reaction/reaction";

export type ReactionsLocalStorage = { [key: string]: Reaction["reaction"] };
